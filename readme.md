readme.md - README for DockAnchor v0.9


Purpose:
Library for docking and anchoring form controls in Qt or Gtk based Gambas UI. Requires GambasAppUtilityLib library 0.4 or later.

Note: Gambas3 projects ('.project') are looking for libraries ('<filename>.gambas') in /home/<user_name>/.local/share/gambas3/lib/<vendor_prefix>/.Make (Project|Make|Executable...) writes the executable there in addition to the root of the project's directory.


Usage notes:

~...


Setup:

~...


0.9: 
~add license file
~add readme
0.8:Qt issue
0.7:ref issue
0.6: internationalization prep
0.5: switch to renamed GambasAppUtilityLib; edit comments
0.4: fix Log.FormatError and calls; switch to AppUtility for Log, Dialogs
0.3: updated to handle docking/anchoring inside container controls.
0.2: updated to handle stacking of docks and awareness of corners in common.
0.1: initial release with docking (opposite sides only recommended) and anchors.

Fixes:

Known Issues:
~

Possible Enhancements:


Steve Sepan
ssepanus@yahoo.com
2/14/2022